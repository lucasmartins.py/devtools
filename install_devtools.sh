#!/bin/sh

# install_devtools.sh
DIR=$(pwd)

# install brew
install_brew(){
    echo "#################"
    echo ""
    if command -v brew; then
        echo "HomeBrew instaled"
    else
        echo "Install HomeBrew"
        /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
        /opt/homebrew/bin/brew install iterm2
        echo "Install Nerd Font"
        /opt/homebrew/bin/brew tap homebrew/cask-fonts
        /opt/homebrew/bin/brew install --cask font-meslo-lg-nerd-font
        echo ""
        echo "export PATH=/opt/homebrew/bin:$PATH" >> ~/.zshrc
        after_install_brew
    fi
}
# install oh-my-zsh
install_oh_my_zsh(){
    echo "#################"
    echo ""
    if [ -d ~/.oh-my-zsh ]; then
        echo " oh-my-zsh instaled"
    else
        echo " Install oh-my-zsh"
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    fi
    after_install
    exit_install
}
# install lmzsh
install_lmzsh(){
    echo "#################"
    echo ""
    if [ -d ~/.config/lmzsh ]; then
        echo "lmzsh instaled"
    else
        echo "Install lmzsh"
        rm -rf ~/.zshrc
        git clone https://gitlab.com/lucasmartins.py/lmzsh.git ~/.config/lmzsh
        ln -s -f ~/.config/lmzsh/zshrc ~/.zshrc
        ln -s -f ~/.config/lmzsh/zsh-colorls ~/.oh-my-zsh/custom/plugins/zsh-colorls
        ln -s -f ~/.config/lmzsh/bin ~/.zshfn
        git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH/plugins/zsh-syntax-highlighting
        git clone https://github.com/romkatv/powerlevel10k.git $ZSH/themes/powerlevel10k
        git clone https://github.com/lukechilds/zsh-nvm.git $ZSH/custom/plugins/zsh-nvm
        ln -s -f ~/.config/lmzsh/p10k.zsh ~/.p10k.zsh
        after_install
        exit_install
    fi
    echo ""
    echo "#################"
}
after_install_brew(){
    echo ""
    echo ""
    echo "Close and open terminal to se changes."
    echo ""
    echo ""
    echo "Open with iterm2"
    echo ""
    echo "Set font to MesloLGS NF in Preferences -> Profiles -> Text -> Font -> MesloLGS NF"
    echo ""
    echo "Rerun ./install_devtools.sh"
    exit
}
after_install(){
    osascript <<EOF
    tell application "iTerm2"
         create window with default profile
         tell current session of current window
              delay 5
              write text "cd $DIR && ./install_devtools.sh"
          end tell
    end tell
EOF
}
exit_install(){
    osascript <<EOF
    tell application "iTerm2"
        tell current window to close current session
    end tell
EOF
}
install_dependencies(){
    echo "Install dependencies"
    brew install zsh zsh-completions ruby rbenv tmux awk perl tmux-mem-cpu-load reattach-to-user-namespace ripgrep nvim pyenv pyenv-virtualenv lua luarocks curl wget gcc fd c llvm glib velero kubectl
    source ~/.zshrc
    gem install colorls
    ln -s -f /opt/homebrew/lib/ruby/gems/3.2.0/bin/colorls /opt/homebrew/opt/ruby/bin/colorls
    echo ""
    echo "Install python3 with pyenv"
    pyenv install 3
    pyenv global 3
    after_install
    exit_install
}
# install lmtmux
install_lmtmux(){
    echo "#################"
    echo ""
    if [ -d ~/.config/lmtmux ]; then
        echo "lmtmux instaled"
    else
        echo "Install lmtmux"
        git clone https://gitlab.com/lucasmartins.py/lmtmux.git ~/.config/lmtmux
        git clone https://github.com/gpakosz/.tmux.git ~/.tmux
        ln -s -f ~/.tmux/.tmux.conf ~/.tmux.conf
        ln -s -f ~/.config/lmtmux/tmux.conf.local ~/.tmux.conf.local
        echo ""
        echo " Configure `$TERM` must be set to `xterm-256color` "
    fi
    echo ""
}
# install nvim
install_nvim(){
    echo "################"
    echo ""
    if [ -d ~/.config/nvim ]; then
        echo "nvim instaled and configured"
    else
        echo "Install nvim"
        echo ""
        echo ""
        sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
        echo ""
        echo ""
        git clone https://gitlab.com/lucasmartins.py/nvim.git ~/.config/nvim
        echo ""
        echo ""
        python3 -m venv ~/.config/nvim/env
        source ~/.config/nvim/env/bin/activate
        pip install --upgrade pip
        pip install wheel pynvim doq flake8 bandit black isort debugpy
        deactivate
        echo ""
        echo "Install nodejs"
        nvm install --lts
        nvm use --lts
        npm i -g pyright neovim dockerfile-language-server-nodejs typescript-language-server typescript vscode-json-languageservice yaml-language-server vscode-langservers-extracted
        echo ""
        echo ""
        nvim -c 'PlugInstall' -c 'qa'
        nvim -c 'UpdateRemotePlugins' -c 'qa'
        ln -s -f ~/.config/nvim/vimspector-config/debug-python.json ~/.local/share/nvim/plugged/vimspector/configurations/macos/_all/debug-python.json
        echo ""
        echo ""
        echo "##############################"
        echo " Configure Wakatime "
        echo " nvim -c 'WakaTimeApiKey' "
        echo ""
        echo " Configure Codeium"
        echo " nvim -c 'Codeium Auth' "
        echo ""
        echo "Install Vimspector gadgets"
        echo " nvim -c 'VimspectorInstall'"
        echo ""
    fi
    echo ""
    echo "################"
}
install_brew
install_oh_my_zsh
install_lmzsh
install_dependencies
install_lmtmux
install_nvim
exit
