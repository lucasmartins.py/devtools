## Install DevTools lm

### Install with zsh, tmux and nvim

```console
cd ~
git clone git@gitlab.com:lucasmartins.py/devtools.git
cd devtools
./install_devtools.sh
```

[@lucasmartins.py](https://gitlab.com/lucasmartins.py)
